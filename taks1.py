# coding utf-8
import pymorphy2

morph = pymorphy2.MorphAnalyzer()

print(morph.parse('руки')[0].normal_form)


def lemms_and_gram_forms(word):
    for el in morph.parse(word):
        print("Normal form: {}, tags: {}, score: {}".format(el.normal_form, str(el.tag), str(el.score)))


lemms_and_gram_forms('руки')
lemms_and_gram_forms('три')


def lexeme_and_score(word):
    for el in morph.parse(word)[0].lexeme:
        print("Word: {}, score {}".format(el.word, el.score))


lexeme_and_score('стать')

turk = morph.parse('турок')[0]
turk = turk.inflect({'plur', 'ablt'})
print(turk)

print(str(morph.parse('майню')[0].tag).split(',')[0])

import re
import collections


def text_to_wordlist(sentence):
    regexp = "[^а-яА-Яё]"
    sentence = re.sub(regexp, " ", sentence)
    result = sentence.lower().split()
    return result


with open('wp.txt', encoding='utf-8') as file:
    text = file.read()
    text = text_to_wordlist(text)
    print("Words count: {}".format(len(text))) # 104369

    text = [morph.parse(el)[0] for el in text]

    nouns = collections.Counter()
    verbs = collections.Counter()
    for word in text:
        if word.tag.POS == "NOUN":
            nouns[word.normal_form] += 1
        elif word.tag.POS == "VERB":
            verbs[word.normal_form] += 1

    print(nouns.most_common()[:10])
    print(verbs.most_common()[:10])

    # [('князь', 876), ('андрей', 404), ('человек', 365), ('лицо', 359), ('пьер', 344), ('рука', 316), ('ростов', 316), ('глаз', 263), ('офицер', 230), ('княжна', 218)]
    # [('быть', 1469), ('сказать', 839), ('говорить', 399), ('мочь', 387), ('знать', 230), ('думать', 167), ('хотеть', 167), ('стать', 130), ('видеть', 128), ('смотреть', 117)]
