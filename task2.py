import collections
import re

import numpy as np

with open('wp.txt', encoding='utf-8') as file:
    text = file.read()


def text_to_wordlist(sentence):
    regexp = "[^а-яА-Яё]"
    sentence = re.sub(regexp, " ", sentence)
    result = sentence.lower().split()
    return result


words = text_to_wordlist(text)


def ngrams(word, n=3):
    w = word
    for _ in range(n - 1):
        w = '#' + w + '#'

    return [w[i:i + n] for i in range(len(word) + n - 1)]


def produce_ngram_dict(source_text, accuracy=0.9):
    counter = collections.Counter()
    ngrams_count = 0
    for word in words:
        for n in range(2, 5):
            for g in ngrams(word, n):
                counter[g] += 1
                ngrams_count += 1

    result_dict = dict()
    for item, count in counter.most_common(int(len(counter) * accuracy)):
        result_dict[item] = count / ngrams_count
    return result_dict


ngrams_dict = produce_ngram_dict(text)


def check_word_by_ngrams(word):
    for n in range(2, 5):
        grams = ngrams(word, n)
        for g in grams:
            if g not in ngrams_dict:
                return False

    return True


assert check_word_by_ngrams("князь")
assert not check_word_by_ngrams("уецноркурц")


class TrieNode:
    def __init__(self):
        self.word = None
        self.count = 0
        self.children = {}

    def insert(self, word):
        node = self
        for symbol in word:
            if symbol not in node.children:
                node.children[symbol] = TrieNode()

            node = node.children[symbol]

        node.word = word
        node.count += 1

    def search(self, word, maxcost, errcount, lastskip=False, lastadd=False):
        if errcount > maxcost:
            return []
        if len(word) == 0 and self.word is not None:
            return [(self.word, errcount, self.count)]

        results = []

        if len(word) > 0:
            first_letter = word[0]

            good_node = self.children.get(first_letter, None)
            if good_node:
                results.extend(good_node.search(word[1:], maxcost, errcount))
        else:
            first_letter = None

        errcount += 1
        if not lastadd:
            # пропуск символа
            results.extend(self.search(word[1:], maxcost, errcount, lastskip=True))

        for letter, node in self.children.items():
            if not lastskip:
                # Вставка символа
                results.extend(node.search(word, maxcost, errcount, lastadd=True))

            # Замена символа
            if letter != first_letter:
                results.extend(node.search(word[1:], maxcost, errcount))

        return results


class SpellChecker:
    def __init__(self, words):
        self.WordCount = 0

        self.trie = TrieNode()
        for word in words:
            self.WordCount += 1
            self.trie.insert(word)

    def search(self, word, max_cost=2):
        results = self.trie.search(word, max_cost, 0)
        if len(results) == 0:
            return None
        words = {word for word, _, _ in results}
        counts = {w: 0 for w in words}
        costs = {w: max_cost for w in words}
        mincost = max_cost
        for word, cost, count in results:
            counts[word] = count
            costs[word] = min(costs[word], cost)
            mincost = min(mincost, cost)

        filtered = [w for w in words if costs[w] == mincost]
        counts_filtered = [counts[word] for word in filtered]
        return filtered[np.argmax(counts_filtered)]


checker = SpellChecker(words)
assert checker.search('князь') == 'князь'
assert checker.search('князъ') == 'князь'
assert checker.search('гнязь') == 'князь'
assert checker.search('знязъ') == 'князь'
assert checker.search('знузъ') != 'князь'
assert checker.search('кушать') == 'кушать'
assert checker.search('кушайь') == 'кушать'
